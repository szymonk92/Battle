package itml;

import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.plot.JFuzzyChart;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 02.10.2015.
 */
public class main2 {
    private static FIS fuzzyInterfaceSystem;
    private static final String FILE_NAME = "fuzzySzymon.fcl";

    private static final String AGENT_COL = "agentCol";
    private static final String AGENT_ROW = "agentRow";
    private static final String AGENT_HEALTH = "agentHealth";
    private static final String AGENT_STAMINA = "agentStamina";

    private static final String OPPONENT_COL = "opponentCol";
    private static final String OPPONENT_ROW = "opponentRow";
    private static final String OPPONENT_HEALTH = "opponentHealth";
    private static final String OPPONENT_STAMINA = "opponentStamina";
    private static final String OPPONENT_POSSIBLE_MOVE = "opponentPossibleMove";

    private static final String DISTANCE = "distance";
    private static final String THIS_SAME_ROW = "thisSameRow";

    private static final String RESULT = "result";

    public static void main(String[] args) {

       System.out.println(Math.sqrt(Math.pow(2 - 4, 2) + Math.pow(3 - 4, 2)));


//        fuzzyInterfaceSystem = FIS.load(FILE_NAME, true);
//        if (fuzzyInterfaceSystem == null) {
//            System.exit(1);
//        }
//
//        List<Double> fuzzyResults = new ArrayList();
//
//        // Get default function block
//        FunctionBlock functionBlock = fuzzyInterfaceSystem
//                .getFunctionBlock(null);
//
//        // Set inputs
//        functionBlock.setVariable(AGENT_COL, 2);
//        functionBlock.setVariable(AGENT_ROW, 3);
//        functionBlock.setVariable(AGENT_HEALTH, 3);
//        functionBlock.setVariable(AGENT_STAMINA, 2);
//
//        functionBlock.setVariable(OPPONENT_COL, 3);
//        functionBlock.setVariable(OPPONENT_ROW, 0);
//        functionBlock.setVariable(OPPONENT_HEALTH, 4);
//        functionBlock.setVariable(OPPONENT_STAMINA, 9);
//
//        functionBlock.setVariable(OPPONENT_POSSIBLE_MOVE, 3);
//
//        functionBlock.setVariable(DISTANCE, 1);
//        functionBlock.setVariable(THIS_SAME_ROW, 1);
//
//        // Evaluate
//        functionBlock.evaluate();
//        functionBlock.getVariable(RESULT).defuzzify();
//        fuzzyResults.add(functionBlock.getVariable(RESULT).getValue());
//        JFuzzyChart.get().chart(functionBlock);
//        System.out.println(functionBlock.getVariable(RESULT).getValue());
    }
}
