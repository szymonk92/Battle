package itml.cards;

/**
 * Created by szymo on 02.10.2015.
 */
public enum CardEnum {

    cAttackCardinal(0),
    cAttackDiagonal(1),
    cAttackLong(2),

    cDefend(3),
    cLeapLeft(4),
    cLeapRight(5),

    cMoveDown(6),
    cMoveLeft(7),
    cMoveRight(8),
    cMoveUp(9),

    cRest(10);

    private final int value;

    CardEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
