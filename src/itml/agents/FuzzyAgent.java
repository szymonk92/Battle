package itml.agents;

import itml.cards.Card;
import itml.cards.CardRest;
import itml.simulator.CardDeck;
import itml.simulator.StateAgent;
import itml.simulator.StateBattle;
import net.sourceforge.jFuzzyLogic.FIS;
import net.sourceforge.jFuzzyLogic.FunctionBlock;
import net.sourceforge.jFuzzyLogic.rule.RuleBlock;
import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by szymo on 02.10.2015.
 */
public class FuzzyAgent extends Agent {
    private int m_noThisAgent;     // Index of our agent (0 or 1).
    private int m_noOpponentAgent; // Inex of opponent's agent.
    private Classifier classifier_;
    private Instances dataset;

    private FIS fuzzyInterfaceSystem;
    private RuleBlock ruleBlock;
    private static final String FILE_NAME = "fuzzySzymon.fcl";

    private static final String AGENT_COL = "agentCol";
    private static final String AGENT_ROW = "agentRow";
    private static final String AGENT_HEALTH = "agentHealth";
    private static final String AGENT_STAMINA = "agentStamina";

    private static final String OPPONENT_COL = "opponentCol";
    private static final String OPPONENT_ROW = "opponentRow";
    private static final String OPPONENT_HEALTH = "opponentHealth";
    private static final String OPPONENT_STAMINA = "opponentStamina";
    private static final String OPPONENT_POSSIBLE_MOVE = "opponentPossibleMove";

    private static final String DISTANCE = "distance";
    private static final String THIS_SAME_ROW = "thisSameRow";
    private static final String NUMBER_OF_MOVES = "numberOfMoves";


    private static final String RESULT = "result";

    ArrayList parameterList = new ArrayList();
    //http://jfuzzylogic.sourceforge.net/html/manual.html

    public FuzzyAgent(CardDeck deck, int msConstruct, int msPerMove, int msLearn) {
        super(deck, msConstruct, msPerMove, msLearn);
        classifier_ = new J48();


        fuzzyInterfaceSystem = FIS.load(FILE_NAME, true);
        if (fuzzyInterfaceSystem == null) {
            System.err.println("Can't load file: '" + FILE_NAME + "'");
            return;
        }

    }

    @Override
    public void startGame(int noThisAgent, StateBattle stateBattle) {
        // Remember the indicies of the agents in the StateBattle.
        m_noThisAgent = noThisAgent;
        m_noOpponentAgent = (noThisAgent == 0) ? 1 : 0; // can assume only 2 agents battling.
    }

    @Override
    public void endGame(StateBattle stateBattle, double[] results) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    @Override
    public Card act(StateBattle stateBattle) {

        return chooseMove(stateBattle);
    }

    private double calculateDistance(int x1, int x2, int y1, int y2) {
        return Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
    }

    private Card chooseMove(StateBattle stateBattle) {
        double[] values = new double[8];
        StateAgent a = stateBattle.getAgentState(0);
        StateAgent o = stateBattle.getAgentState(1);
        values[0] = a.getCol();
        values[1] = a.getRow();
        values[2] = a.getHealthPoints();
        values[3] = a.getStaminaPoints();
        values[4] = o.getCol();
        values[5] = o.getRow();
        values[6] = o.getHealthPoints();
        values[7] = o.getStaminaPoints();

        double distance = calculateDistance(a.getCol(), o.getCol(), a.getRow(), o.getRow());
        int thisSameRow = (a.getRow() == o.getRow()) ? 1 : 0;

        try {
            ArrayList<Card> allCards = m_deck.getCards();
            ArrayList<Card> cards = m_deck.getCards(a.getStaminaPoints());
            Instance i = new Instance(1.0, values.clone());
            i.setDataset(dataset);
            int out = (int) classifier_.classifyInstance(i);

            //choose action
            int fuzzyResult = action(values, out, distance, thisSameRow);
            Card selected = allCards.get(fuzzyResult);

            return selected;

        } catch (Exception e) {
            System.out.println("Error classifying new instance: " + e.toString());
        }
        return new CardRest();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Classifier learn(Instances instances) {
        this.dataset = instances;
        try {
            classifier_.buildClassifier(instances);
        } catch (Exception e) {
            System.out.println("Error training classifier: " + e.toString());
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private int action(double[] values, int predicted, double distance, int thisSameRow) {

        List<Double> fuzzyResults = new ArrayList();

        // Get default function block
        FunctionBlock functionBlock = fuzzyInterfaceSystem.getFunctionBlock(null);

        // Set inputs
        functionBlock.setVariable(AGENT_COL, values[0]);
        functionBlock.setVariable(AGENT_ROW, values[1]);
        functionBlock.setVariable(AGENT_HEALTH, values[2]);
        functionBlock.setVariable(AGENT_STAMINA, values[3]);

        functionBlock.setVariable(OPPONENT_COL, values[4]);
        functionBlock.setVariable(OPPONENT_ROW, values[5]);
        functionBlock.setVariable(OPPONENT_HEALTH, values[6]);
        functionBlock.setVariable(OPPONENT_STAMINA, values[7]);

        functionBlock.setVariable(OPPONENT_POSSIBLE_MOVE, predicted);

        functionBlock.setVariable(DISTANCE, distance);
        functionBlock.setVariable(THIS_SAME_ROW, thisSameRow);

        // Evaluate
        functionBlock.evaluate();
        functionBlock.getVariable(RESULT).defuzzify();
        fuzzyResults.add(functionBlock.getVariable(RESULT).getValue());
        System.out.println(functionBlock.getVariable(RESULT).getValue());

        return fuzzyResults.get(fuzzyResults.size() - 1).intValue();
    }
}
