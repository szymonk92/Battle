package itml.agents;

import itml.cards.*;
import itml.simulator.CardDeck;
import itml.simulator.StateAgent;
import itml.simulator.StateBattle;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.classifiers.trees.J48;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Random;

/**
 * User: deong
 * Date: 9/28/14
 */
public class SzymonAgent extends Agent {
    private int m_noThisAgent;     // Index of our agent (0 or 1).
    private int m_noOpponentAgent; // Inex of opponent's agent.
    private Classifier classifier_;
    private Instances dataset;


    public SzymonAgent(CardDeck deck, int msConstruct, int msPerMove, int msLearn) {
        super(deck, msConstruct, msPerMove, msLearn);
        MultilayerPerceptron multilayerPerceptron = new MultilayerPerceptron();
        multilayerPerceptron.setLearningRate(0.6);
        multilayerPerceptron.setMomentum(0.3);
        multilayerPerceptron.setTrainingTime(500);
        multilayerPerceptron.setValidationThreshold(20);
        //'a' = (number of attributes + number of classes) / 2
        //'i' = number of attributes,
        //'o' = number of classes,
        //'t' = number of attributes + number of classes.
        multilayerPerceptron.setHiddenLayers("a");
        classifier_ = multilayerPerceptron;

    }

    @Override
    public void startGame(int noThisAgent, StateBattle stateBattle) {
        // Remember the indicies of the agents in the StateBattle.
        m_noThisAgent = noThisAgent;
        m_noOpponentAgent = (noThisAgent == 0) ? 1 : 0; // can assume only 2 agents battling.
    }

    @Override
    public void endGame(StateBattle stateBattle, double[] results) {

        //To change body of implemented methods use File | Settings | File Templates.
    }

    Random m_random = new Random();

    @Override
    public Card act(StateBattle stateBattle) {
        Card opponentMove = predictOpponentMove(stateBattle);
        StateAgent stateAgent = stateBattle.getAgentState(m_noThisAgent);
        StateAgent opponentAgent = stateBattle.getAgentState(m_noOpponentAgent);

        ArrayList<Card> cards = m_deck.getCards(stateAgent.getStaminaPoints());
        ArrayList<Card> allCards = m_deck.getCards();


        if (dataset == null) {
            return terminator(stateBattle, stateAgent, opponentAgent, cards);
        } else {

            return weHaveData(stateBattle, opponentMove, cards, allCards);
        }

    }

    private Card searchCardByName(ArrayList<Card> cards, String name) {
        for (Card card : cards) {
            if (card.getName().equals(name)) {
                return card;
            }
        }
        return null;
    }

    private Card weHaveData(StateBattle stateBattle, Card opponentMove, ArrayList<Card> cards, ArrayList<Card> allCards) {
        Card cDefend = searchCardByName(cards, "cDefend");
        Card cAttackCardinal = searchCardByName(cards, "cAttackCardinal");
        Card cAttackDiagonal = searchCardByName(cards, "cAttackDiagonal");
        Card cAttackLong = searchCardByName(cards, "cAttackLong");
        Card cLeapLeft = searchCardByName(cards, "cLeapLeft");
        Card cLeapRight = searchCardByName(cards, "cLeapRight");
        Card cMoveDown = searchCardByName(cards, "cMoveDown");
        Card cMoveLeft = searchCardByName(cards, "cMoveLeft");
        Card cMoveRight = searchCardByName(cards, "cMoveRight");
        Card cMoveUp = searchCardByName(cards, "cMoveUp");
        Card cRest = searchCardByName(cards, "cRest");

        StateAgent agentMy = stateBattle.getAgentState(m_noThisAgent);
        StateAgent agentOpp = stateBattle.getAgentState(m_noOpponentAgent);


        if (agentMy.getStaminaPoints() == 0) {
            return new CardRest();

        } else if (opponentMove.getType() == Card.CardActionType.ctAttack) {
            return opponentAttacks(opponentMove, stateBattle, agentMy, agentOpp);

        } else if (opponentMove.getType() == Card.CardActionType.ctMove) {
            //ATTACK!!!!
            //BIT HIM IN CORNER
            // if (opponentMove == cRest) { //REST
            if (agentMy.getStaminaPoints() > 2) {
                return terminator(stateBattle, agentMy, agentOpp, cards);
            } else {
                return new CardRest();
            }

        } else if (opponentMove.getType() == Card.CardActionType.ctDefend) {
            return new CardRest();
        } else {
            return null; // null pointer exception possible
        }

    }

    private Card opponentAttacks(Card opponentMove, StateBattle stateBattle, StateAgent agentMy, StateAgent agentOpp) {
        double distance = calcDistanceBetweenAgents(stateBattle);

        if (opponentMove.getName() == "cAttackCardinal") {
            if (distance == 1) { //ATTACK RANGE
                if ((agentMy.getCol() == 0 && (agentMy.getRow() == 0 || agentMy.getRow() == stateBattle.getNumRows() - 1))
                        || agentMy.getCol() == stateBattle.getNumColumns() - 1 &&
                        (agentMy.getRow() == 0 || agentMy.getRow() == stateBattle.getNumRows() - 1)) {
                    return new CardDefend();
                } else {
                    return chickenCard(stateBattle);
                }
            } else { //out of attack range
                return new CardDefend();
            }
        } else if (opponentMove.getName() == "cAttackDiagonal") {
            if (distance == Math.sqrt(2)) { //ATTACK RANGE
                if ((agentMy.getCol() == 0 && (agentMy.getRow() == 0 || agentMy.getRow() == stateBattle.getNumRows() - 1))
                        || agentMy.getCol() == stateBattle.getNumColumns() - 1 &&
                        (agentMy.getRow() == 0 || agentMy.getRow() == stateBattle.getNumRows() - 1)) {
                    return new CardDefend();
                } else {
                    return chickenCard(stateBattle);
                }
            } else { //out of attack range
                return new CardDefend();
            }
        } else if (opponentMove.getName() == "cAttackLong") {
            Card[] moveUpDown = new Card[]{new CardMoveDown(), new CardMoveDown()};
            if (agentMy.getRow() == agentOpp.getRow() && calcDistanceBetweenAgents(stateBattle) <= 2) {
                if (agentMy.getRow() == 0) {
                    return new CardMoveDown();
                } else if (agentMy.getRow() == stateBattle.getNumRows() - 1) {
                    return new CardMoveUp();
                } else {
                    return moveUpDown[new Random().nextInt(moveUpDown.length)];
                }
            } else {
                if (agentMy.getStaminaPoints() < 4) {
                    return new CardRest();
                } else {
                    return moveUpDown[new Random().nextInt(moveUpDown.length)];
                }
            }
        } else {
            System.out.println("ERROR >> ERROR >> ERROR >> ERROR >> ERROR");
            ArrayList<Card> cards = m_deck.getCards(agentMy.getStaminaPoints());
            return cards.get(m_random.nextInt(cards.size()));
        }
    }

    private Card chickenCard(StateBattle stateBattle) {
        Card[] move = new Card[2];

        move[m_noOpponentAgent] = new CardRest();   // We assume the opponent just stays where he/she is,
        // and then take the move that brings us as far away as possible.

        Card bestCard = new CardRest();
        double minDistance = calcDistanceBetweenAgents(stateBattle);

        ArrayList<Card> cards = m_deck.getCards(stateBattle.getAgentState(m_noThisAgent).getStaminaPoints());
        for (Card card : cards) {
            StateBattle bs = (StateBattle) stateBattle.clone();   // close the state, as play( ) modifies it.
            move[m_noThisAgent] = card;
            bs.play(move);
            double distance = calcDistanceBetweenAgents(bs);
            if (distance > minDistance) {
                bestCard = card;
                minDistance = distance;
            }
        }

        return bestCard;
    }

    private Card terminator(StateBattle stateBattle, StateAgent stateAgent, StateAgent opponentAgent, ArrayList<Card> cards) {
        // First check to see if we are in attack range, if so attack.
        for (Card card : cards) {

            if (stateAgent.getStaminaPoints() <= 1
                    && card.inAttackRange(stateAgent.getCol(), stateAgent.getRow(),
                    opponentAgent.getCol(), opponentAgent.getRow())) {
                return new CardRest();

            }
            if ((card.getType() == Card.CardActionType.ctAttack)


                    && card.inAttackRange(stateAgent.getCol(), stateAgent.getRow(),
                    opponentAgent.getCol(), opponentAgent.getRow())
                    && stateAgent.getStaminaPoints() >= 2) {
                return card;  // attack!
            }
        }

        // If we cannot attack, then try to move closer to the agent.
        Card bestCard = goCloser(stateBattle, cards);

        return bestCard;
    }

    private Card goCloser(StateBattle stateBattle, ArrayList<Card> cards) {
        Card[] move = new Card[2];
        move[m_noOpponentAgent] = new CardRest();

        Card bestCard = new CardRest();
        double bestDistance = calcDistanceBetweenAgents(stateBattle);

        // ... otherwise move closer to the opponent.
        for (Card card : cards) {
            StateBattle bs = (StateBattle) stateBattle.clone();   // close the state, as play( ) modifies it.
            move[m_noThisAgent] = card;
            bs.play(move);
            double distance = calcDistanceBetweenAgents(bs);
            if (distance < bestDistance) {
                bestCard = card;
                bestDistance = distance;
            }
        }
        return bestCard;
    }

    private Card predictOpponentMove(StateBattle stateBattle) {
        double[] values = new double[8];
        StateAgent a = stateBattle.getAgentState(0);
        StateAgent o = stateBattle.getAgentState(1);
        values[0] = a.getCol();
        values[1] = a.getRow();
        values[2] = a.getHealthPoints();
        values[3] = a.getStaminaPoints();
        values[4] = o.getCol();
        values[5] = o.getRow();
        values[6] = o.getHealthPoints();
        values[7] = o.getStaminaPoints();
        try {
            ArrayList<Card> allCards = m_deck.getCards();
            ArrayList<Card> cards = m_deck.getCards(a.getStaminaPoints());
            Instance i = new Instance(1.0, values.clone());
            i.setDataset(dataset);
            int out = (int) classifier_.classifyInstance(i);
            Card selected = allCards.get(out);
            if (cards.contains(selected)) {
                return selected;
            }
        } catch (Exception e) {
            System.out.println("Error classifying new instance: " + e.toString());
        }
        return new CardRest();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Classifier learn(Instances instances) {
        this.dataset = instances;
        try {
            classifier_.buildClassifier(instances);
            Evaluation m_Evaluation = new Evaluation(instances);
            m_Evaluation.crossValidateModel(
                    classifier_,
                    instances,
                    4,
                    instances.getRandomNumberGenerator(1));
            System.out.println(m_Evaluation.toSummaryString());
        } catch (Exception e) {
            System.out.println("Error training classifier: " + e.toString());
        }
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    private double calcDistanceBetweenAgents(StateBattle bs) {

        StateAgent opponent = bs.getAgentState(m_noOpponentAgent);
        StateAgent agent = bs.getAgentState(m_noThisAgent);

        return Math.sqrt(Math.pow(opponent.getCol() - agent.getCol(), 2) + Math.pow(opponent.getRow() - agent.getRow(), 2));
    }
}